/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 10.1.19-MariaDB : Database - blog
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`blog` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_bin */;

USE `blog`;

/*Table structure for table `blog_admin` */

DROP TABLE IF EXISTS `blog_admin`;

CREATE TABLE `blog_admin` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) COLLATE utf8_bin NOT NULL COMMENT '用户名',
  `password` varchar(32) COLLATE utf8_bin NOT NULL COMMENT '密码',
  `loginnum` int(11) NOT NULL DEFAULT '0' COMMENT '登陆次数',
  `last_login_ip` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '最后登录IP',
  `last_login_time` int(11) DEFAULT NULL COMMENT '最后登录时间',
  `status` int(11) DEFAULT NULL COMMENT '状态',
  `typeid` int(11) NOT NULL COMMENT '用户角色id',
  `nick` varchar(100) COLLATE utf8_bin NOT NULL COMMENT '昵称',
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `blog_admin` */

insert  into `blog_admin`(`admin_id`,`username`,`password`,`loginnum`,`last_login_ip`,`last_login_time`,`status`,`typeid`,`nick`) values (1,'admin','e10adc3949ba59abbe56e057f20f883e',20,'::1',1498009262,1,1,'野老盟客');

/*Table structure for table `blog_node` */

DROP TABLE IF EXISTS `blog_node`;

CREATE TABLE `blog_node` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `node_name` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '节点名称',
  `module_name` varbinary(255) NOT NULL COMMENT '模块名',
  `control_name` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '控制器名',
  `action_name` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '方法名',
  `is_menu` int(11) NOT NULL DEFAULT '0' COMMENT '是否是菜单项 1不是 2是',
  `typeid` int(11) NOT NULL COMMENT '父级节点id',
  `style` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '菜单样式',
  `route` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '路由链接地址',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `blog_node` */

insert  into `blog_node`(`id`,`node_name`,`module_name`,`control_name`,`action_name`,`is_menu`,`typeid`,`style`,`route`) values (1,'首页','admin','IndexController','index',2,0,'','admin/index'),(2,'系统设置','admin','IndexController','index',2,0,'',''),(3,'退出','admin','LoginController','quit',1,1,'','admin/quit'),(4,'获取验证码','admin','LoginController','code',1,0,'','admin/code'),(9,'用户管理','admin','SystemController','user',2,2,'','admin/system/user'),(10,'角色管理','admin','SystemController','role',2,2,'','admin/system/role'),(11,'节点添加','admin','SystemController','node',2,2,'','admin/system/node'),(12,'添加节点','admin','SystemController','nodeAdd',1,11,'','admin/system/nodeAdd'),(13,'修改密码','admin','LoginController','pass',1,1,'','admin/pass'),(14,'节点修改','admin','SystemController','nodeEdit',1,11,'','admin/system/nodeEdit'),(15,'节点删除','admin','SystemController','nodeDel',1,11,'','admin/system/nodeDel'),(17,'用户添加','admin','SystemController','userAdd',1,9,'','admin/system/userAdd'),(18,'用户删除','admin','SystemController','userDel',1,9,'','admin/system/userDel'),(19,'用户修改','admin','SystemController','userEdit',1,9,'','admin/system/userEdit'),(20,'角色添加','admin','SystemController','roleAdd',1,10,'','admin/system/roleAdd'),(21,'角色删除','admin','SystemController','roleDel',1,10,'','admin/system/roleDel'),(22,'角色修改','admin','SystemController','roleEdit',1,10,'','admin/system/roleEdit'),;

/*Table structure for table `blog_role` */

DROP TABLE IF EXISTS `blog_role`;

CREATE TABLE `blog_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rolename` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '角色名称',
  `rule` text COLLATE utf8_bin NOT NULL COMMENT '权限节点数据',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `blog_role` */

insert  into `blog_role`(`id`,`rolename`,`rule`) values (1,'超级管理员',''),(3,'管理员','1,3,13,2,9,17,18,19');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
