<!DOCTYPE html>
<html>
  
  <head>
    <meta charset="utf-8" />
    <title>野老盟客后台管理</title>
    <meta name="renderer" content="webkit" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <link rel="icon" type="image/png" href="assets/i/favicon.png" />
    <link rel="apple-touch-icon-precomposed" href="assets/i/app-icon72x72@2x.png" />
    <meta name="apple-mobile-web-app-title" content="Amaze UI" />
    <link rel="stylesheet" href="{{asset('resources/views/admin/assets/css/amazeui.min.css')}}" />
    <link rel="stylesheet" href="{{asset('resources/views/admin/assets/css/admin.css')}}" />
    <link rel="stylesheet" href="{{asset('resources/views/admin/assets/css/app.css')}}" />
    <script src="{{asset('resources/views/admin/assets/js/echarts.min.js')}}"></script>
    <style type="text/css">
      .self-nav{
        padding: 4px 14px 9px 48px;
      }
    </style>
  </head>
  
  <body data-type="index">
    <!--开始头-->
    <header class="am-topbar am-topbar-inverse admin-header">
      <div class="am-topbar-brand">
        <a href="javascript:;" class="tpl-logo">
          <img src="{{asset('resources/views/index/images/logo.png')}}" alt="" /></a>
      </div>
      <div class="am-icon-list tpl-header-nav-hover-ico am-fl am-margin-right"></div>
      <button class="am-topbar-btn am-topbar-toggle am-btn am-btn-sm am-btn-success am-show-sm-only" data-am-collapse="{target: '#topbar-collapse'}">
        <span class="am-sr-only">导航切换</span>
        <span class="am-icon-bars"></span>
      </button>
      <div class="am-collapse am-topbar-collapse" id="topbar-collapse">
        <ul class="am-nav am-nav-pills am-topbar-nav am-topbar-right admin-header-list tpl-header-list">
          <li class="am-dropdown" data-am-dropdown="" data-am-dropdown-toggle="">
            <a class="am-dropdown-toggle tpl-header-list-link" href="javascript:;">
              <span class="am-icon-bell-o"></span>提醒
              <span class="am-badge tpl-badge-success am-round">5</span></a>
            <ul class="am-dropdown-content tpl-dropdown-content">
              <li class="tpl-dropdown-content-external">
                <h3>你有
                  <span class="tpl-color-success">5</span>条提醒</h3>
                <a href="###">全部</a></li>
            </ul>
          </li>
          <li class="am-hide-sm-only">
            <a href="javascript:;" id="admin-fullscreen" class="tpl-header-list-link">
              <span class="am-icon-arrows-alt"></span>
              <span class="admin-fullText">开启全屏</span></a>
          </li>
          <li class="am-dropdown" data-am-dropdown="" data-am-dropdown-toggle="">
            <a class="am-dropdown-toggle tpl-header-list-link" href="javascript:;">
              <span class="tpl-header-list-user-nick">{{$nick}}</span>
              <span class="tpl-header-list-user-ico">
                <img style="width: 39px;" src="{{asset('resources/views/index/images/03.jpg')}}" /></span>
            </a>
            <ul class="am-dropdown-content">
              <li>
                <a href="{{url('admin/pass')}}">
                  <span class="am-icon-cog"></span>修改密码</a>
              </li>
              <!-- <li>
                <a href="#">
                  <span class="am-icon-power-off"></span>退出</a>
              </li> -->
            </ul>
          </li>
          <li>
            <a href="{{url('admin/quit')}}" class="tpl-header-list-link">
              <span class="am-icon-sign-out tpl-header-list-ico-out-size"></span>
            </a>
          </li>
        </ul>
      </div>
    </header>
    <!--结尾头-->

    <div class="tpl-page-container tpl-page-header-fixed">
      <div class="tpl-left-nav tpl-left-nav-hover">
        <div class="tpl-left-nav-title">后台管理功能列表</div>
        <div class="tpl-left-nav-list">
          <ul class="tpl-left-nav-menu">
           @foreach ($menu as $m)
            <li class="tpl-left-nav-item">
              
              @if(count($m['child'])>0)
                <a href="javascript:;" class="nav-link tpl-left-nav-link-list">
                  <i class="{{ $m['style'] }}"></i>
                  <span>{{ $m['name'] }}</span>
                  <i class="am-icon-angle-right tpl-left-nav-more-ico am-fr am-margin-right"></i>
                </a>
                <ul class="tpl-left-nav-sub-menu">
                @foreach ($m['child'] as $mm)
                  <li>


                    @if(count($mm['child'])>0)
                      <a href="javascript:;" class="tpl-left-nav-link-list">
                        <i class="{{ $m['style'] }}"></i>
                        <span>{{ $mm['name'] }}</span>
                        <i class="am-icon-angle-right tpl-left-nav-more-ico am-fr am-margin-right"></i>
                      </a>
                      <ul class="tpl-left-nav-sub-menu">
                      @foreach ($mm['child'] as $mmm)
                        <li>
                          <a href="{{ url($mmm['route']) }}" style="padding: 4px 14px 9px 48px">
                            <i class="{{ $mmm['style'] }}"></i>
                            <span>{{ $mmm['name'] }}</span>
                          </a>
                        </li>
                      @endforeach
                      </ul>
                      @else
                      <a href="{{ url($mm['route']) }}">
                        <i class="{{ $mm['style'] }}"></i>
                        <span>{{ $mm['name'] }}</span>
                      </a>
                    @endif
                  </li>
                @endforeach
                </ul>
                
                @else
                <a href="{{ url($m['route']) }}" class="nav-link">
                  <i class="{{ $m['style'] }}"></i>
                  <span>{{ $m['name'] }}</span>
                </a>
              @endif
              

            </li>
          @endforeach
            
          </ul>
        </div>
      </div>
      
      <div class="tpl-content-wrapper">
        <div style="position: absolute;top: 1.6rem;z-index: 9999;" class="tpl-content-page-title"> 博客后台系统 
          <ol class="am-breadcrumb" style="display: inline-block;margin-left: 2rem;font-size: 14px">
            <li>
              <a href="#" class="am-icon-home">首页</a>
            </li>
            @yield('tab')
          </ol>
        </div>
          
        <div style="margin-top: -0.9rem;">
        <!-- 内容和填充区 -->
         @yield('content')
         </div>
      </div>

    </div>

    <style type="text/css">
    .pagination{
        position: relative;
        padding-left: 0;
        margin: 1.5rem 0;
        list-style: none;
        color: #999;
        text-align: left;
        padding: 0;
    }
    .pagination li{
        float: left;
        display: inline-block;
        margin: 0;
        padding: 0;
        list-style: none;
    }

     .pagination li>a ,.pagination li>span{
        position: relative;
        display: block;
        padding: .5em 1em;
        text-decoration: none;
        line-height: 1.2;
        background-color: #fff;
        border: 1px solid #ddd;
        border-radius: 0;
        margin-bottom: 5px;
        margin-right: 5px;
        color: #23abf0;
        border-radius: 3px;
        padding: 6px 12px;
    }
    .pagination .active{
        margin: 0;
        padding: 0;
        list-style: none;
        display: inline-block;
    }
    .pagination .active>span{
        position: relative;
        display: block;
        text-decoration: none;
        line-height: 1.2;
        margin-bottom: 5px;
        margin-right: 5px;
        border-radius: 3px;
        z-index: 2;
        background-color: #0e90d2;
        border-color: #0e90d2;
        cursor: default;
        background: #23abf0;
        color: #fff;
        border: 1px solid #23abf0;
        padding: 6px 12px;
    }
    .my_table{
      text-align:center;
      font-size: 1.4rem;
    }
    .my_table a{
      background: #fff;
    }
</style>

    <script src="{{asset('resources/views/admin/assets/js/jquery.min.js')}}"></script>
    <script src="{{asset('resources/views/admin/assets/js/amazeui.min.js')}}"></script>
    <script src="{{asset('resources/views/admin/assets/js/iscroll.js')}}"></script>
    <script src="{{asset('resources/views/admin/assets/js/app.js')}}"></script>
    <script type="text/javascript" src="{{asset('resources/org/layer/layer.js')}}"></script>

</body>
  @yield('script')

</html>