@extends('layouts.admin')
@section('tab') <li><a href="{{url('admin/system/role')}}">角色列表</a></li> <li><a>新增角色</a></li> @endSection
@section('content')

<link rel="StyleSheet" href="{{asset('resources/org/tree/dtree.css')}}" type="text/css" />
<script type="text/javascript" src="{{asset('resources/org/tree/dtree.js')}}"></script>

<div class="tpl-portlet-components" style="overflow: visible;">
    <div class="portlet-title">
        <div class="caption font-green bold">
            <span class="am-icon-code"></span> 新增角色
        </div>
        <div class="tpl-portlet-input tpl-fz-ml">
            <div class="portlet-input input-small input-inline">
                <div class="input-icon right">
                    <i class="am-icon-search"></i>
                    <input type="text" class="form-control form-control-solid" placeholder="搜索..."> </div>
            </div>
        </div>
    </div>
    <div class="tpl-block ">
        <div class="am-g tpl-amazeui-form">
            <div class="am-u-sm-12 am-u-md-9">
                <form id="formid" class="am-form am-form-horizontal" action="" method="">
                    {{csrf_field()}}
                     <div style="text-align: center;">
                    @if(count($errors)>0)
                        @if(is_array($errors))
                            @foreach($errors as $error)
                              @foreach($error as $e)
                                  <p style="font-size: 18px;color: red;display: inline-block;">{{$e}}</p>
                              @endforeach
                            @endforeach
                        @else
                             <p style="font-size: 18px;color: red;text-align: center;">{{$errors}}</p>
                        @endif
                        
                    @endif
                    </div>
                    <div class="am-form-group">
                        <label for="user-name" class="am-u-sm-3 am-form-label">角色名</label>
                        <div class="am-u-sm-9">
                            <input type="text" name="role_name" placeholder="节点名">
                            <small>角色名字！</small>
                        </div>
                    </div>
                    <div class="am-form-group">
                        <label for="user-name" class="am-u-sm-3 am-form-label">角色权限</label>
                        <div class="am-u-sm-9">

                        
                        <div class="dtree">
                            <p>
                                <a href="javascript:  d.closeAll();">打开所有权限</a> |
                                <a href="javascript: d.openAll();">关闭所有权限</a>
                            </p>
                            <script type="text/javascript">
                                d = new dTree('d');
                                d.add(0, -1, '权限管理');
                                @foreach($node as $n)
                                    d.add("{{ $n['id'] }}", "{{ $n['pid'] }}", 'autho', "{{ $n['id'] }}", "{{ $n['nodeName'] }}");
                                @endforeach
                                d.openAll();
                                document.write(d);

                                </script>
                            </div>  


                            <input type="hidden" name="role" id="role"/>
                            <small>就是该角色具有的权限</small>
                        </div>
                    </div>

                    <div class="am-form-group">
                        <div class="am-u-sm-9 am-u-sm-push-3">
                            <input type="submit" value="新增角色" class="am-btn am-btn-primary"></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script type="text/javascript">
        $('#formid').click(function(){
            var count = 0;
            var obj = document.all.autho;
            var str = '';
            for (i = 0; i < obj.length; i++) {
                if (obj[i].checked) {
                    str += obj[i].value+",";
                    count++;
                }
            }
            str = str.substring(0,str.length-1);
            $('#role').val(str);
            return true;
        });
    </script>
@endsection