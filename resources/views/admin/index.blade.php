@extends('layouts.admin')
@section('tab') <li><a href="#">分类</a></li><li class="am-active">内容</li> @endSection
@section('content')
    
    <div class="tpl-content-scope">
  <div class="note note-info">
    <h3>野老盟客
      <span class="close" data-close="note" /></h3>
    <p>收集一切可以收集的东西</p>
    <p>
  </div>
  <div class="row">
    <div class="am-u-lg-3 am-u-md-6 am-u-sm-12">
      <div class="dashboard-stat blue">
        <div class="visual">
          <i class="am-icon-comments-o"></i>
        </div>
        <div class="details">
          <div class="number">1349</div>
          <div class="desc">新的回复</div></div>
        <a class="more" href="#">查看更多
          <i class="m-icon-swapright m-icon-white"></i></a>
      </div>
    </div>
    <div class="am-u-lg-3 am-u-md-6 am-u-sm-12">
      <div class="dashboard-stat red">
        <div class="visual">
          <i class="am-icon-bar-chart-o"></i>
        </div>
        <div class="details">
          <div class="number">62%</div>
          <div class="desc">浏览人数</div></div>
        <a class="more" href="#">查看更多
          <i class="m-icon-swapright m-icon-white"></i></a>
      </div>
    </div>
    <div class="am-u-lg-3 am-u-md-6 am-u-sm-12">
      <div class="dashboard-stat green">
        <div class="visual">
          <i class="am-icon-apple"></i>
        </div>
        <div class="details">
          <div class="number">653</div>
          <div class="desc">注册人数</div></div>
        <a class="more" href="#">查看更多
          <i class="m-icon-swapright m-icon-white"></i></a>
      </div>
    </div>
    <div class="am-u-lg-3 am-u-md-6 am-u-sm-12">
      <div class="dashboard-stat purple">
        <div class="visual">
          <i class="am-icon-android"></i>
        </div>
        <div class="details">
          <div class="number">786</div>
          <div class="desc">收集文章数</div></div>
        <a class="more" href="#">查看更多
          <i class="m-icon-swapright m-icon-white"></i></a>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="am-u-md-6 am-u-sm-12 row-mb">
      <div class="tpl-portlet">
        <div class="tpl-portlet-title">
          <div class="tpl-caption font-green ">
            <i class="am-icon-cloud-download"></i>
            <span>浏览人数 数据统计</span></div>
          <div class="actions">
            <ul class="actions-btn">
              <li class="red-on">昨天</li>
              <li class="green">前天</li>
              <li class="blue">本周</li></ul>
          </div>
        </div>
        <!--此部分数据请在 js文件夹下中的 app.js 中的 “百度图表A” 处修改数据 插件使用的是 百度echarts-->
        <div class="tpl-echarts" id="tpl-echarts-A"></div>
      </div>
    </div>
    <div class="am-u-md-6 am-u-sm-12 row-mb">
      <div class="tpl-portlet">
        <div class="tpl-portlet-title">
          <div class="tpl-caption font-red ">
            <i class="am-icon-bar-chart"></i>
            <span>回复 动态资料</span>
          </div>
          <div class="actions">
            <ul class="actions-btn">
              <li class="purple-on">昨天</li>
              <li class="green">前天</li>
              <li class="dark">本周</li></ul>
          </div>
        </div>
        <div class="tpl-scrollable">
          <div class="number-stats">
            <div class="stat-number am-fl am-u-md-6">
              <div class="title am-text-right">Total</div>
              <div class="number am-text-right am-text-warning">2460</div></div>
            <div class="stat-number am-fr am-u-md-6">
              <div class="title">Total</div>
              <div class="number am-text-success">2460</div></div>
          </div>
          <table class="am-table tpl-table">
            <thead>
              <tr class="tpl-table-uppercase">
                <th>名字</th>
                <th>内容</th>
                <th>时间</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  <a class="user-name" href="###">禁言小张</a></td>
                <td>￥3213</td>
                <td>65</td>
              </tr>
              <tr>
                <td>
                  <a class="user-name" href="###">Alex.</a></td>
                <td>￥2635</td>
                <td>52</td>
              </tr>
              <tr>
                <td>
                  <a class="user-name" href="###">Tinker404</a></td>
                <td>￥1267</td>
                <td>65</td>
              </tr>
              <tr>
                <td>
                  <a class="user-name" href="###">Arron.y</a></td>
                <td>￥657</td>
                <td>65</td>
              </tr>
              <tr>
                <td>
                  <a class="user-name" href="###">Yves</a></td>
                <td>￥3907</td>
                <td>65</td>
              </tr>
              <tr>
                <td>
                  <a class="user-name" href="###">小黄鸡</a></td>
                <td>￥900</td>
                <td>65</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>


@endsection