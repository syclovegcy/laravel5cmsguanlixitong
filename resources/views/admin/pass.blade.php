@extends('layouts.admin')
@section('tab') <li><a href="#">修改密码</a></li> @endSection
@section('content')
    

  
  
        <div class="tpl-portlet-components">
          <div class="portlet-title">
            <div class="caption font-green bold">
              <span class="am-icon-code"></span>密码修改</div>
          </div>
          <div class="tpl-block ">
            <div class="am-g tpl-amazeui-form">
              <div class="am-u-sm-12 am-u-md-9">
                <form method="post" action="" class="am-form am-form-horizontal">
                {{csrf_field()}}
                 <div style="text-align: center;">
                @if(count($errors)>0)
                    @if(is_array($errors))
                        @foreach($errors as $error)
                          @foreach($error as $e)
                              <p style="font-size: 18px;color: red;display: inline-block;">{{$e}}</p>
                          @endforeach
                        @endforeach
                    @else
                         <p style="font-size: 18px;color: red;text-align: center;">{{$errors}}</p>
                    @endif
                    
                @endif
                </div>

                  <div class="am-form-group">
                    <label for="user-name" class="am-u-sm-3 am-form-label">原密码 / password</label>
                    <div class="am-u-sm-9">
                      <input type="password" id="user-name" name="opwd" placeholder="原密码 / password">
                      <small>输入原密码，以便验证。</small>
                    </div>
                  </div>
                  <div class="am-form-group">
                    <label for="user-name" class="am-u-sm-3 am-form-label">新密码 / password</label>
                    <div class="am-u-sm-9">
                      <input type="password" id="user-name" name="npwd" placeholder="新密码 / password">
                      <small>输入新密码,需要6到20位。</small>
                    </div>
                  </div>
                  <div class="am-form-group">
                    <label for="user-name" class="am-u-sm-3 am-form-label">再一次输入 / password</label>
                    <div class="am-u-sm-9">
                      <input type="password" id="user-name" name="npwd_confirmation" placeholder="再一次输入 / password">
                      <small>再一次输入，需要与新密码一致。</small>
                    </div>
                  </div>
                  <div class="am-form-group">
                    <div class="am-u-sm-9 am-u-sm-push-3">
                      <button type="submit" class="am-btn am-btn-primary">保存修改</button></div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
@endsection