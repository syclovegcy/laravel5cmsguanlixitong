@extends('layouts.admin')
@section('tab') <li><a href="#">后台用户列表</a></li> @endSection
@section('content')

            <div class="tpl-portlet-components">
                <div class="tpl-block">
                    <div class="am-g">
                        <div class="am-u-sm-12 am-u-md-6">
                            <div class="am-btn-toolbar">
                                <div class="am-btn-group am-btn-group-xs" style="margin-bottom: 0.5rem;">
                                    <a type="button" href="{{ url('admin/system/userAdd')}}" class="am-btn am-btn-default am-btn-success"><span class="am-icon-plus"></span> 新增</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="am-g">
                        <div class="am-u-sm-12">
                            <form class="am-form">
                                <table class="am-table am-table-striped am-table-hover am-table-bordered am-table-radius">
                                    <thead>
                                        <tr>
                                            <!-- <th class="table-check"><input type="checkbox" class="tpl-table-fz-check"></th> -->
                                            <th class="my_table">ID</th>
                                            <th class="my_table">用户名</th>
                                            <th class="my_table">登录帐号</th>
                                            <th class="my_table">登录ip</th>
                                            <th class="my_table">用户权限</th>
                                            <th class="my_table">操作</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($user as $v)
                                        <tr>
                                            <!-- <td><input type="checkbox"></td> -->
                                            <td class="my_table">{{ $v->admin_id}}</td>
                                            <td class="my_table">{{ $v->nick}}</td>
                                            <td class="my_table">{{ $v->username }}</td>
                                            <td class="my_table">{{ $v->last_login_ip }}</td>
                                            <td class="my_table">{{ $v->rolename }}</td>
                                            <td class="my_table">
                                                <div class="am-btn-group am-btn-group-xs">
                                                    <a href="{{ url('admin/system/userEdit/'.$v->admin_id) }}" class="am-btn am-btn-default am-btn-xs am-text-secondary"><span class="am-icon-pencil-square-o"></span> 编辑</a>
                                                    <a class="am-btn am-btn-default am-btn-xs am-text-danger am-hide-sm-only" href="javascript:;" onclick="delUser({{$v->admin_id}})"><span class="am-icon-trash-o"></span> 删除</a>
                                                </div>
                                            </td>
                                        </tr>
                                     @endforeach

                                    </tbody>
                                </table>
                                <div class="am-cf">

                                    <div class="am-fr">
                                        {{ $user->links() }}
                                        
                                    </div>
                                </div>

                                <hr>

                            </form>
                        </div>

                    </div>
                </div>
                <div class="tpl-alert"></div>
            </div>

    <script type="text/javascript">
        function delUser(id){
            layer.confirm('您确定要删除这个用户吗？', {
            btn: ['确定','取消'] //按钮
        }, function(){
            $.post("{{url('admin/system/userDel')}}/"+id,{'_token':"{{csrf_token()}}"},function (data) {
                if(data.status==0){
                    location.href = location.href;
                    layer.msg(data.msg, {icon: 6});
                }else{
                    layer.msg(data.msg, {icon: 5});
                }
            });
           // layer.msg('的确很重要', {icon: 1});
        }, function(){

        });
        }
    </script>
@endsection