@extends('layouts.admin')
@section('tab') <li><a href="{{url('admin/system/user')}}">后台用户列表</a></li> <li><a>新增后台用户</a></li> @endSection
@section('content')


<div class="tpl-portlet-components" style="overflow: visible;">
    <div class="portlet-title">
        <div class="caption font-green bold">
            <span class="am-icon-code"></span> 新增后台用户
        </div>
        <div class="tpl-portlet-input tpl-fz-ml">
            <div class="portlet-input input-small input-inline">
                <div class="input-icon right">
                    <i class="am-icon-search"></i>
                    <input type="text" class="form-control form-control-solid" placeholder="搜索..."> </div>
            </div>
        </div>
    </div>
    <div class="tpl-block ">
        <div class="am-g tpl-amazeui-form">
            <div class="am-u-sm-12 am-u-md-9">
                <form class="am-form am-form-horizontal" action="" method="">
                    {{csrf_field()}}
                     <div style="text-align: center;">
                    @if(count($errors)>0)
                        @if(is_array($errors))
                            @foreach($errors as $error)
                              @foreach($error as $e)
                                  <p style="font-size: 18px;color: red;display: inline-block;">{{$e}}</p>
                              @endforeach
                            @endforeach
                        @else
                             <p style="font-size: 18px;color: red;text-align: center;">{{$errors}}</p>
                        @endif
                        
                    @endif
                    </div>
                    <div class="am-form-group">
                        <label for="user-name" class="am-u-sm-3 am-form-label">昵称 / nick</label>
                        <div class="am-u-sm-9">
                            <input type="text" name="nick" placeholder="昵称 / nick">
                            <small>输入昵称，以后要是文章直接显示！</small>
                        </div>
                    </div>
                    <div class="am-form-group">
                        <label for="user-name" class="am-u-sm-3 am-form-label">用户名 / Username</label>
                        <div class="am-u-sm-9">
                            <input type="text" name="username" placeholder="用户名 / Username">
                            <small>输入用户名，以后可以登录哦！</small>
                        </div>
                    </div>
                    <div class="am-form-group">
                        <label for="user-email" class="am-u-sm-3 am-form-label">密码 / Password</label>
                        <div class="am-u-sm-9">
                            <input type="password" name="password" placeholder="密码 / Password">
                            <small>密码，是一个比较隐私的哦( ′◔ ‸◔`)！</small>
                        </div>
                    </div>

                     <div class="am-form-group">
                        <label for="user-phone" class="am-u-sm-3 am-form-label">状态
                            <span class="tpl-form-line-small-title">Status</span>
                        </label>
                        <div class="am-u-sm-9">
                            <label class="am-radio-inline">
                                <input type="radio"  value="0" name="status"> 禁用
                            </label>
                            <label class="am-radio-inline">
                                <input type="radio" value="1" checked="checked" name="status"> 启用
                            </label>
                        </div>
                    </div>

                    <div class="am-form-group">
                        <label for="user-phone" class="am-u-sm-3 am-form-label">角色
                            <span class="tpl-form-line-small-title">Role</span>
                        </label>
                        <div class="am-u-sm-9">
                            <select name="role" data-am-selected="{searchBox: 1}">
                                @foreach($role as $v)
                                    <option value="{{$v->id}}">{{$v->rolename}}</option>
                                @endforeach
                               
                            </select>
                        </div>
                    </div>

                    <div class="am-form-group">
                        <div class="am-u-sm-9 am-u-sm-push-3">
                            <input type="submit" value="新增用户" class="am-btn am-btn-primary"></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



@endsection

