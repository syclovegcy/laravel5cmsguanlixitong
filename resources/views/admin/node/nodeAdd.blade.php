@extends('layouts.admin')
@section('tab') <li><a href="{{url('admin/system/node')}}">节点列表</a></li> <li><a>新增节点</a></li> @endSection
@section('content')


<div class="tpl-portlet-components" style="overflow: visible;">
    <div class="portlet-title">
        <div class="caption font-green bold">
            <span class="am-icon-code"></span> 新增节点
        </div>
        <div class="tpl-portlet-input tpl-fz-ml">
            <div class="portlet-input input-small input-inline">
                <div class="input-icon right">
                    <i class="am-icon-search"></i>
                    <input type="text" class="form-control form-control-solid" placeholder="搜索..."> </div>
            </div>
        </div>
    </div>
    <div class="tpl-block ">
        <div class="am-g tpl-amazeui-form">
            <div class="am-u-sm-12 am-u-md-9">
                <form class="am-form am-form-horizontal" action="" method="">
                    {{csrf_field()}}
                     <div style="text-align: center;">
                    @if(count($errors)>0)
                        @if(is_array($errors))
                            @foreach($errors as $error)
                              @foreach($error as $e)
                                  <p style="font-size: 18px;color: red;display: inline-block;">{{$e}}</p>
                              @endforeach
                            @endforeach
                        @else
                             <p style="font-size: 18px;color: red;text-align: center;">{{$errors}}</p>
                        @endif
                        
                    @endif
                    </div>
                    <div class="am-form-group">
                        <label for="user-name" class="am-u-sm-3 am-form-label">节点名</label>
                        <div class="am-u-sm-9">
                            <input type="text" name="node_name" placeholder="节点名">
                            <small>节点名字，可以设置为菜单！</small>
                        </div>
                    </div>
                    <div class="am-form-group">
                        <label for="user-name" class="am-u-sm-3 am-form-label">模块名</label>
                        <div class="am-u-sm-9">
                            <input type="text" name="module_name" placeholder="模块名">
                            <small>就是后台模块是否需要权限</small>
                        </div>
                    </div>
                    <div class="am-form-group">
                        <label for="user-email" class="am-u-sm-3 am-form-label">控制器名</label>
                        <div class="am-u-sm-9">
                            <input type="text" name="control_name" placeholder="控制器名">
                            <small>控制器，你懂得</small>
                        </div>
                    </div>
                    <div class="am-form-group">
                        <label for="user-email" class="am-u-sm-3 am-form-label">方法名</label>
                        <div class="am-u-sm-9">
                            <input type="text" name="action_name" placeholder="方法名">
                            <small>调用的方法</small>
                        </div>
                    </div>

                    <div class="am-form-group">
                        <label for="user-email" class="am-u-sm-3 am-form-label">菜单样式</label>
                        <div class="am-u-sm-9">
                            <input type="text" name="style" placeholder="菜单样式">
                            <small>菜单样式</small>
                        </div>
                    </div>

                    <div class="am-form-group">
                        <label for="user-email" class="am-u-sm-3 am-form-label">路由</label>
                        <div class="am-u-sm-9">
                            <input type="text" name="route" placeholder="路由">
                            <small>路由</small>
                        </div>
                    </div>

                     <div class="am-form-group">
                        <label for="user-phone" class="am-u-sm-3 am-form-label">菜单
                            <span class="tpl-form-line-small-title">是否是菜单</span>
                        </label>
                        <div class="am-u-sm-9">
                            <label class="am-radio-inline">
                                <input type="radio"  value="2" name="is_menu"> 是
                            </label>
                            <label class="am-radio-inline">
                                <input type="radio" value="1" checked="checked" name="is_menu"> 否
                            </label>
                        </div>
                    </div>

                    <div class="am-form-group">
                        <label for="user-phone" class="am-u-sm-3 am-form-label">父级节点
                            <span class="tpl-form-line-small-title">最多二级</span>
                        </label>
                        <div class="am-u-sm-9">
                            <select name="typeid" data-am-selected="{searchBox: 1}">
                                <option value="0">首级菜单</option>
                                @foreach($node as $v)
                                    <option value="{{$v->id}}">{{$v->node_name}}</option>
                                @endforeach
                               
                            </select>
                        </div>
                    </div>

                    <div class="am-form-group">
                        <div class="am-u-sm-9 am-u-sm-push-3">
                            <input type="submit" value="新增节点" class="am-btn am-btn-primary"></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



@endsection

