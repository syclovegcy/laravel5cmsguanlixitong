@extends('layouts.admin')
@section('tab') <li><a href="#">节点列表</a></li> @endSection
@section('content')

            <div class="tpl-portlet-components">
                <div class="tpl-block">
                    <div class="am-g">
                        <div class="am-u-sm-12 am-u-md-6">
                            <div class="am-btn-toolbar">
                                <div class="am-btn-group am-btn-group-xs" style="margin-bottom: 0.5rem;">
                                    <a type="button" href="{{ url('admin/system/nodeAdd')}}" class="am-btn am-btn-default am-btn-success"><span class="am-icon-plus"></span> 新增</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="am-g">
                        <div class="am-u-sm-12">
                            <form class="am-form">
                                <table class="am-table am-table-striped am-table-hover am-table-bordered am-table-radius">
                                    <thead>
                                        <tr>
                                            <!-- <th class="table-check"><input type="checkbox" class="tpl-table-fz-check"></th> -->
                                            <th class="my_table">ID</th>
                                            <th class="my_table">节点名</th>
                                            <th class="my_table">模块名</th>
                                            <th class="my_table">控制器名</th>
                                            <th class="my_table">方法名</th>
                                            <th class="my_table">操作</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($node as $v)
                                        <tr>
                                            <!-- <td><input type="checkbox"></td> -->
                                            <td class="my_table">{{ $v->id }}</td>
                                            <td class="my_table">{{ $v->node_name }}</td>
                                            <td class="my_table">{{ $v->module_name }}</td>
                                            <td class="my_table">{{ $v->control_name }}</td>
                                            <td class="my_table">{{ $v->action_name }}</td>
                                            <td class="my_table">
                                                <div class="am-btn-group am-btn-group-xs">
                                                    <a href="{{ url('admin/system/nodeEdit/'.$v->id) }}" class="am-btn am-btn-default am-btn-xs am-text-secondary"><span class="am-icon-pencil-square-o"></span> 编辑</a>
                                                    <a class="am-btn am-btn-default am-btn-xs am-text-danger am-hide-sm-only" href="javascript:;" onclick="delUser({{$v->id}})"><span class="am-icon-trash-o"></span> 删除</a>
                                                </div>
                                            </td>
                                        </tr>
                                     @endforeach

                                    </tbody>
                                </table>
                                <div class="am-cf">

                                    <div class="am-fr">
                                        {{ $node->links() }}
                                        
                                    </div>
                                </div>

                                <hr>

                            </form>
                        </div>

                    </div>
                </div>
                <div class="tpl-alert"></div>
            </div>

    <script type="text/javascript">
        function delUser(id){
            layer.confirm('您确定要删除这个菜单节点吗？', {
            btn: ['确定','取消'] //按钮
        }, function(){
            $.post("{{url('admin/system/nodeDel')}}/"+id,{'_token':"{{csrf_token()}}"},function (data) {
                if(data.status==0){
                    location.href = location.href;
                    layer.msg(data.msg, {icon: 6});
                }else{
                    layer.msg(data.msg, {icon: 5});
                }
            });
           // layer.msg('的确很重要', {icon: 1});
        }, function(){

        });
        }
    </script>
@endsection