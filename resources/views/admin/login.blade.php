<!doctype html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>野老盟客后台登录</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
  <link rel="icon" type="image/png" href="{{asset('resources/views/admin/assets/i/favicon.png')}}">
  <link rel="apple-touch-icon-precomposed" href="{{asset('resources/views/admin/assets/i/app-icon72x72@2x.png')}}">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
  <link rel="stylesheet" href="{{asset('resources/views/admin/assets/css/amazeui.min.css')}}" />
  <link rel="stylesheet" href="{{asset('resources/views/admin/assets/css/admin.css')}}">
  <link rel="stylesheet" href="{{asset('resources/views/admin/assets/css/app.css')}}">
</head>

<body data-type="login">

  <div class="am-g myapp-login">
	<div class="myapp-login-logo-block  tpl-login-max">
		<div class="myapp-login-logo-text">
			<div class="myapp-login-logo-text">
				野老盟客后台<span> 登录</span> <i class="am-icon-skyatlas"></i>

			</div>
		</div>

		<div class="am-u-sm-10 login-am-center">
			<form class="am-form" action="" method="post">
                @if(session('msg'))
                <p style="color: #dd514c;margin-left: 1.2rem;text-align: center;">{{session('msg')}}</p>
                @endif
                {{csrf_field()}}
				<fieldset>
					<div class="am-form-group">
						<input type="text" name="username" id="doc-ipt-email-1" placeholder="来个帐号">
					</div>
					<div class="am-form-group">
						<input type="password" name="password" style="border-radius: 0px;" class="" id="doc-ipt-pwd-1" placeholder="输入密码吧！">
					</div>
                    <div class="am-form-group">
                        <input type="text" name="code" style="width: 100%;float: left;    border-radius: 0px 0px 6px 6px;" class="" id="doc-ipt-pwd-1" placeholder="你信不信验证码是假的">
                        <img src="{{url('admin/code')}}" style="width: 13.434rem;height:3.34rem;position: relative;border-radius: 0px 0px 6px 0px;top: -3.5rem ;left: 26.2rem;" alt="" onclick="this.src='{{url('admin/code')}}?'+Math.random()">
                    </div>
					<p><button type="submit" class="am-btn am-btn-default">登录</button></p>
				</fieldset>
			</form>
		</div>
	</div>
</div>

  <script src="{{asset('resources/views/admin/assets/js/jquery.min.js')}}"></script>
  <script src="{{asset('resources/views/admin/assets/js/amazeui.min.js')}}"></script>
  <script src="{{asset('resources/views/admin/assets/js/app.js')}}"></script>
</body>

</html>