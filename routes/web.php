<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['web']], function () {

    Route::get('/', function () {
        return view('welcome');
    });

    Route::any('admin/login', 'Admin\LoginController@login');
    Route::get('admin/code', 'Admin\LoginController@code');
});
 Route::get('index', 'IndexController@index');

Route::group(['middleware' => ['web','adminLogin'],'prefix'=>'admin','namespace'=>'Admin'], function () {
    //首页路由
    Route::get('index', 'IndexController@index');

    //退出和修改密码路由
    Route::get('quit', 'LoginController@quit');
    Route::any('pass', 'LoginController@pass');
    
    //权限管理路由
    Route::group(['prefix'=>'system'], function () {
        //用户管理
        Route::any('user','SystemController@user');
        Route::any('userAdd','SystemController@userAdd');
        Route::any('userDel/{uid}','SystemController@userDel');
        Route::any('userEdit/{uid}','SystemController@userEdit');

        //权限管理
        Route::any('role','SystemController@role');
        Route::any('roleAdd','SystemController@roleAdd');
        Route::any('roleDel/{uid}','SystemController@roleDel');
        Route::any('roleEdit/{uid}','SystemController@roleEdit');
        
        //节点管理
        Route::any('node','SystemController@node');
        Route::any('nodeAdd','SystemController@nodeAdd');
        Route::any('nodeDel/{uid}','SystemController@nodeDel');
        Route::any('nodeEdit/{uid}','SystemController@nodeEdit');
    });

    
    Route::post('cate/changeorder', 'CategoryController@changeOrder');
    Route::resource('category', 'CategoryController');

    Route::resource('article', 'ArticleController');

    Route::any('upload', 'CommonController@upload');

});
