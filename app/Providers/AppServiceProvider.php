<?php

namespace App\Providers;

use App\Http\Utils\HelperUtils;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('admin/*',function($view){
            $menu = Session()->get("menu");
            $nick = Session()->get("nick");
            // dd($menu);
            $view->with(compact('menu','nick'));
        });
        // $menu = Cache::get('menu');
        // dd($menu);
        // view()->share('menu',$menu);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
