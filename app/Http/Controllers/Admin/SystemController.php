<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Utils\HelperUtils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

/** 系统设置 */
class SystemController extends Controller{

	/** 用户管理页面 */
	function user(){
		$user = DB::table('admin')->join('role',function($join){
			$join->on('admin.typeid','=','role.id');
		})->paginate(10);
		$data['user'] = $user;
		return view('admin.user.user',$data);
	}


	/** 新增用户 */
	function userAdd(){
		$role = DB::table('role')->get()->toArray();
		if($input = Input::all()){
			$rules = [
                'password'=>'required|between:6,20',
                'username'=>'required|between:5,20',
                'nick'=>'required',
                'role'=>'required'
            ];
            $message = [
                'password.required' => '密码不能为空',
                'password.between' => '密码必须设置6到20位之间',
                'username.required' => '帐号不能为空',
                'username.between' => '帐号必须设置5到20位之间',
                'nick.required' => '昵称不能为空',
                'role.required' => '角色不能为空',
            ];
            $validator = Validator::make($input,$rules,$message);
            	// dd($input);

            if($validator->passes()){
            	//通过，进行保存
            	$data['username'] = $input['username'];
            	$data['password'] = md5($input['password']);
            	$data['typeid'] = $input['role'];
            	$data['status'] = $input['status'];
            	$data['nick'] = $input['nick'];
            	$data['loginnum'] = '0';
            	$data['last_login_time'] = time();
            	$result = DB::table('admin')->where('username',$data['username'])->get()->toArray();
            	if(count($result)>0){
            		$arr = '帐号重复，请重新选择';
					return back()->with("errors",$arr);
            	}else{
            		DB::table('admin')->insert($data);
            		return redirect('admin/system/user');
            	}
            }else{
                $arr = [];
                $msg=$validator->messages()->messages();
                foreach ($msg as $key => $value) {
                    $arr[$key] = $value;
                }
                return back()->with("errors",$arr);
            }
		}
		return view('admin.user.userAdd',compact('role'));
	}

	/** 删除用户 */
	function userDel($uid){
		$result = DB::table("admin")->where('admin_id',$uid)->delete();
		if($result){
			$data = [
                'status' => 0,
                'msg' => '删除成功',
            ];
		}else{
			$data = [
                'status' => 1,
                'msg' => '删除失败',
            ];
		}
		return $data;
	}

    /** 修改用户 */
	function userEdit($uid){
		$result = DB::table('admin')->where('admin_id',$uid)->take(1)->get()->toArray();
		$role = DB::table('role')->get()->toArray();
		$data['role'] = $role;
		$data['result']=$result[0];
        if($input = Input::all()){
            //通过，进行保存
            $uid = $input['uid'];
            if(!empty($input['password'])){
                $data['password'] = md5($input['password']);
            }
            $d['typeid'] = $input['role'];
            $d['status'] = $input['status'];
            $d['nick'] = $input['nick'];
            $uu = DB::table('admin')->where("admin_id",$uid)->update($d);
            return redirect('admin/system/user');
        }
		return view('admin.user.userEdit',$data);
	}

	/** 节点添加页面 */
	function node(){
		$node = DB::table('node')->where('id','<>','1')->orderBy('id','desc')->paginate(10);
        $data['node'] = $node;
        return view('admin.node.index',$data);
	}

    /** node节点增加 */
    function nodeAdd(){
        $node = DB::table('node')->where('id','<>','1')->where('is_menu','2')->orderBy('id','desc')->get()->toArray();
        // $node = DB::table('node')->where('id','<>','1')->where('typeid','=','0')->orderBy('id','desc')->get()->toArray();
        if($input = Input::all()){
            $rules = [
                'node_name'=>'required',
                'is_menu'=>'required',
                'route'=>'required'
            ];
            $message = [
                'node_name.required' => '节点名不能为空',
                'is_menu.required' => '菜单选项不能为空',
                'route.required' => '路由不能为空',
            ];
            $validator = Validator::make($input,$rules,$message);

            if($validator->passes()){
                //通过，进行保存
                $data['node_name'] = $input['node_name'];
                $data['module_name'] = $input['module_name'];
                $data['control_name'] = $input['control_name'];
                $data['action_name'] = $input['action_name'];

                $data['is_menu'] = $input['is_menu'];
                $data['typeid'] = $input['typeid'];
                $data['style'] = empty($input['style'])?'':$input['style'];
                $data['route'] = $input['route'];

                DB::table('node')->insert($data);
                
                $help = new HelperUtils();
                $help->getNodeRole();
                
                return redirect('admin/system/node');
            }else{
                $arr = [];
                $msg=$validator->messages()->messages();
                foreach ($msg as $key => $value) {
                    $arr[$key] = $value;
                }
                return back()->with("errors",$arr);
            }
        }
        return view('admin.node.nodeAdd',compact('node'));
    }

    /** 删除节点 */
    function nodeDel($uid){
        $result = DB::table("node")->where('id',$uid)->delete();
        if($result){
            $data = [
                'status' => 0,
                'msg' => '删除成功',
            ];
        }else{
            $data = [
                'status' => 1,
                'msg' => '删除失败',
            ];
        }
        $help = new HelperUtils();
        $help->getNodeRole();
        return $data;
    }

    /** 修改节点 */
    function nodeEdit($uid){
        $result = DB::table('node')->where('id',$uid)->take(1)->get()->toArray();
        $node = DB::table('node')->where('id','<>','1')->where('is_menu','2')->get()->toArray();
        // $node = DB::table('node')->where('id','<>','1')->where('typeid','=','0')->get()->toArray();
        $data['node'] = $node;
        $data['result']=$result[0];
        if($input = Input::all()){
            //通过，进行保存
            $uid = $input['id'];
                
            $d['node_name'] = $input['node_name'];
            $d['module_name'] = $input['module_name'];
            $d['control_name'] = $input['control_name'];
            $d['action_name'] = $input['action_name'];

            $d['is_menu'] = $input['is_menu'];
            $d['typeid'] = $input['typeid'];
            $d['style'] = empty($input['style'])?'':$input['style'];
            $d['route'] = $input['route'];

            $uu = DB::table('node')->where("id",$uid)->update($d);
            $help = new HelperUtils();
            $help->getNodeRole();
            return redirect('admin/system/node');
        }
        return view('admin.node.nodeEdit',$data);
    }


    /** 权限管理页面 */
    function role(){
        $role = DB::table('role')->orderBy('id','desc')->paginate(10);
        $data['role'] = $role;
        return view('admin.role.index',$data);
    }

    /** role角色增加 */
    function roleAdd(){
        //获取所有的节点，然后组装
        $node = DB::table('node')->get()->toArray();
        $helper = new HelperUtils();
        $node = $helper->getNodes($node);
        if($input = Input::all()){
            $rules = [
                'role_name'=>'required',
            ];
            $message = [
                'role_name.required' => '角色名不能为空',
            ];
            $validator = Validator::make($input,$rules,$message);

            if($validator->passes()){
                //通过，进行保存
                $data['rolename'] = $input['role_name'];
                $data['rule'] = $input['role'];

                DB::table('role')->insert($data);
                return redirect('admin/system/role');
            }else{
                $arr = [];
                $msg=$validator->messages()->messages();
                foreach ($msg as $key => $value) {
                    $arr[$key] = $value;
                }
                return back()->with("errors",$arr);
            }
        }
        return view('admin.role.add',compact('node'));
    }

    /** 删除角色 */
    function roleDel($uid){
        $result = DB::table("role")->where('id',$uid)->delete();
        if($result){
            $data = [
                'status' => 0,
                'msg' => '删除成功',
            ];
        }else{
            $data = [
                'status' => 1,
                'msg' => '删除失败',
            ];
        }
        $help = new HelperUtils();
        $help->getNodeRole();
        return $data;
    }

    /** 修改角色 */
    function roleEdit($uid){
        //获取所有的节点，然后组装
        $node = DB::table('node')->get()->toArray();
        $helper = new HelperUtils();
        $node = $helper->getNodes($node);
        $role = DB::table('role')->where('id',$uid)->take(1)->get()->toArray();

        $myNode = explode(',',$role[0]->rule);
        foreach ($node as $key => $value) {
            if(in_array($value['id'],$myNode)){
                $node[$key]['isCheck'] = true;
            }else{
                $node[$key]['isCheck'] = false;
            }
        }
        if($input = Input::all()){
            //通过，进行保存
            $uid = $input['id'];
                
            $d['rolename'] = $input['role_name'];
            $d['rule'] = $input['role'];

            $uu = DB::table('role')->where("id",$uid)->update($d);
            return redirect('admin/system/role');
        }
        $data['role'] = $role[0];
        $data['node'] = $node;
        return view('admin.role.edit',$data);
    }
}
