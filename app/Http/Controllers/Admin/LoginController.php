<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Utils\HelperUtils;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
require_once 'resources/org/code/Code.class.php';

class LoginController extends Controller{

    //登录
    function login(Request $request){
        if($input = Input::all()){
            $code = new \Code;
            $_code = $code->get();
            if(strtoupper($input['code'])!=$_code){
                return back()->with('msg','验证码错误');
            }
            $data['username'] = $input['username'];
            $admin = DB::table('admin')->where($data)->first();
            if(empty($admin)||$admin->password!=md5($input['password'])){
                return back()->with('msg','用户名或者密码错误！');
            }
            if($admin->status == "0"){
                return back()->with('msg','用户已经禁用了！');
            }
            
            //通过typeid去获取当前用户权限
            if($admin->typeid != 0 ){
                //说明不是超级管理员
                $type = DB::table('role')->where('id',$admin->typeid)->first();
                //保存权限记录和规则
                $request->session()->put('type', $type);
                $request->session()->put('rule', $type->rule);
            }
            //将当前信息保存起来
            $login['last_login_ip'] = $request->getClientIp();
            $login['last_login_time'] = time();
            $login['loginnum'] = $admin->loginnum;

            DB::table('admin')->where('admin_id',$admin->admin_id)->update($login);
            //获取md5认证登录记录
            $request->session()->put('user', md5($admin->username.$admin->password));
            $request->session()->put('username', $admin->username);
            $request->session()->put('typeid', $admin->typeid);
            $request->session()->put('nick', $admin->nick);
            $request->session()->put('time', time());
            //获取权限
            $help = new HelperUtils();
            $help->getNodeRole();
            //保存在缓存中
            return redirect('admin/index');
        }else {
            return view('admin.login');
        }
    }

    public function code()
    {   
        $code = new \Code;
        $code->make();
    }

    /** 退出操作 */
    public function quit(){
        session(['user'=>null]);
        session(['username'=>null]);
        session(['typeid'=>null]);

        session(['time'=>null]);
        session(['type'=>null]);
        session(['rule'=>null]);
        return redirect('admin/login');
    }

    /** 修改密码 */
    public function pass(Request $request){
        if($input = Input::all()){
            $rules = [
                'npwd'=>'required|between:6,20|confirmed',
                'npwd_confirmation'=>'required|between:6,20'
            ];
            $message = [
                'npwd.required' => '新密码不能为空',
                'npwd.between' => '密码必须设置6到20位之间',
                'npwd.confirmed' => '两次输入的新密码必须一致',
                'npwd_confirmation.required' => '新密码不能为空',
                'npwd_confirmation.between' => '密码必须设置6到20位之间',
            ];
            $validator = Validator::make($input,$rules,$message);
            if($validator->passes()){
                $username = $request->session()->get('username');
                $admin = DB::table('admin')->where('username',$username)->first();
                if(md5($input['opwd']) != $admin->password){
                    return back()->with('errors','原密码错误');
                }else{
                    $pass['password'] = md5($input['npwd']);
                    $r = DB::table('admin')->where('admin_id',$admin->admin_id)->update($pass);
                    if($r){
                        return redirect('admin/index');
                    }else{
                        return back()->with('errors','更新失败，请稍后重试!');
                    }
                }
            }else{
                $arr = [];
                $msg=$validator->messages()->messages();
                foreach ($msg as $key => $value) {
                    $arr[$key] = $value;
                }
                return back()->with("errors",$arr);
            }
        }else{
            return view('admin.pass');
        }
    }
}
