<?php 
namespace App\Http\Utils;

use Illuminate\Support\Facades\DB;
/**
 * 用户帮助类
 */
class HelperUtils{
	//登出删除sesion操作
	function logout(){
		session(['user'=>null]);
        session(['username'=>null]);
        session(['typeid'=>null]);
            
        session(['time'=>null]);
        session(['type'=>null]);
        session(['rule'=>null]);
	}

	/** 重新获取权限 */
	public function getNodeRole(){
		$typeid = Session()->get('typeid');
        //获取权限
        if($typeid == '1'){
            //如果是0，获取所有的页面权限
            $nodes = DB::table('node')->select()->get()->toArray();
            //将获取到的所有菜单保存起来
            Session()->put('node', $nodes);
        }else{
            $rule = DB::table('role')->where('id','=',$typeid)->select('rule')->first();
            $n = explode(',',$rule->rule);
            $nodes = DB::table('node')->whereIn('id',$n)->select()->get()->toArray();
            //将获取到的所有菜单保存起来
            Session()->put('node', $nodes);
        }
	    //这里开始组装菜单
        $menu = $this->getMenu($nodes);
        //保存在session中
        Session()->put('menu', $menu);
	}

	/** 返回模块名 */
	public function getModelName(){
		$model = $this->getCurrentAction()['controller']; 
		$data = explode("\\",$model);
		return $data[count($data)-2];
	}
	/** 返回只有控制器 */
	public function getOneControllerName(){
		$controller = $this->getCurrentAction()['controller']; 
		$class = class_basename($controller);
		return $class;
	}
	/** 返回当前控制器 */
	public function getCurrentControllerName(){
		return $this->getCurrentAction()['controller'];  
	}

	/** 获取当前方法名  */
	public function getCurrentMethodName()  
	{  
	    return $this->getCurrentAction()['method'];  
	}  
	 
	/** 获取当前控制器与方法   */
	public function getCurrentAction()  
	{  
	    $action = \Route::current()->getActionName();  
	    list($class, $method) = explode('@', $action);  
	 
	    return ['controller' => $class, 'method' => $method];  
	}  

	//打印出当前sql
	public function getSQL($db){
		$db->enableQueryLog();  
	    $log = DB::getQueryLog();
	    dd($log);   //打印sql语句
	    die;
	}

	/**
	 * 数组 转 对象
	 */
	function array_to_object($arr) {
	    if (gettype($arr) != 'array') {
	        return;
	    }
	    foreach ($arr as $k => $v) {
	        if (gettype($v) == 'array' || getType($v) == 'object') {
	            $arr[$k] = (object)array_to_object($v);
	        }
	    }
	    return (object)$arr;
	}
 
	/**
	 * 对象 转 数组
	 * 后续优化
	 */
	function object_to_array($obj) {
	    $obj = (array)$obj;
	    foreach ($obj as $k => $v) {
	        if (gettype($v) == 'resource') {
	            return;
	        }
	        if (gettype($v) == 'object' || gettype($v) == 'array') {
	            $obj[$k] = (array)object_to_array($v);
	        }
	    }
	    return $obj;
	}

	/**
	 * 通过nodes 返回menu
	 */
	function getMenu($nodes){
		if(is_array($nodes)){
			$menu = [];
			$num = [];
			//如果是节点，开始组装菜单
			//第一步:所有typeid一样的放到一起
			foreach ($nodes as $key => $value) {
				if($value->is_menu ==1){
					continue;
				}
				$m = [];
				$m['id'] = $key+1;
				$m['name'] = $value->node_name;
				$m['route'] = $value->route;
				$m['pid'] = $value->typeid;
				$m['style'] = $value->style;
				$m['child'] = [];
				// $menu[$key] = $m;
				$num[$value->typeid]=isset($num[$value->typeid])?  $num[$value->typeid]+1 :0; 
				$menu[$value->typeid][$num[$value->typeid]] = $m;
			}
			//第二步骤:执行核心菜单模块
			$menu = array_collapse($menu);
			$tree =[];
			$tree = $this->getMenuCore($menu);
			return $tree;
		}

		//如果不是数组，表示没有节点不正确
		return false;
	}

	/**
	 * 返回菜单核心代码
	 */
	function getMenuCore($data){  
		$menu = array();
		foreach ($data as $v) {
		    $menu[$v['id']] = $v;
		    $menu[$v['id']]['child'] = array();//items存放当前节点的所有子节点。
		    if($v['pid'] != 0) {
		        $menu[$v['pid']]['child'][count($menu[$v['pid']]['child'])] = &$menu[$v['id']];
		    }
		}
		foreach ($menu as $k=>$v) {
		    if($v['pid'] != 0) {
		        unset($menu[$k]);
		    }
		}
		return $menu;
	}  

	//返回组装的数据结构
	function getNodes($node){
		$nodes = [];
		foreach ($node as $key => $value) {
			$nodes[$key]['id'] = $value->id;
			$nodes[$key]['pid'] = $value->typeid;
			$nodes[$key]['nodeName'] = $value->node_name;
		}
		return $nodes;
	}


}